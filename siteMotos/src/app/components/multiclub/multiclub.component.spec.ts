import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MulticlubComponent } from './multiclub.component';

describe('MulticlubComponent', () => {
  let component: MulticlubComponent;
  let fixture: ComponentFixture<MulticlubComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MulticlubComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MulticlubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
