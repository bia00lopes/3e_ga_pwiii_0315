import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcompanhamentoPedidosComponent } from './acompanhamento-pedidos.component';

describe('AcompanhamentoPedidosComponent', () => {
  let component: AcompanhamentoPedidosComponent;
  let fixture: ComponentFixture<AcompanhamentoPedidosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcompanhamentoPedidosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcompanhamentoPedidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
