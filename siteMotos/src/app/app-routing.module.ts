import { HomeComponent } from './components/home/home.component';
import { MulticlubComponent } from './components/multiclub/multiclub.component';
import { StreetComponent } from './components/street/street.component';
import { SportComponent } from './components/sport/sport.component';
import { ScooterComponent } from './components/scooter/scooter.component';
import { TrailComponent } from './components/trail/trail.component';
import { TouringComponent } from './components/touring/touring.component';
import { CustomComponent } from './components/custom/custom.component';
import { AcompanhamentoPedidosComponent } from './components/acompanhamento-pedidos/acompanhamento-pedidos.component';
import { CadastroClientesComponent } from './components/cadastro-clientes/cadastro-clientes.component';
import { CadastroMotosComponent } from './components/cadastro-motos/cadastro-motos.component';
import { CadastroUsuariosComponent } from './components/cadastro-usuarios/cadastro-usuarios.component';
import { LoginComponent } from './components/login/login.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; 


const routes: Routes = [
  {path:"", component: HomeComponent},
  {path:"multiclub", component: MulticlubComponent},
  {path:"street", component: StreetComponent},
  {path:"sport", component: SportComponent},
  {path:"scooter", component: ScooterComponent},
  {path:"trail", component: TrailComponent},
  {path:"touring", component: TouringComponent},
  {path:"custom", component: CustomComponent},
  {path:"acompanhemento", component: AcompanhamentoPedidosComponent},
  {path:"cad-clientes", component: CadastroClientesComponent},
  {path:"cad-motos", component: CadastroMotosComponent},
  {path:"cad-usuarios", component: CadastroUsuariosComponent},
  {path:"login", component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
